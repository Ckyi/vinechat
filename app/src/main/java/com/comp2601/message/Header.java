package com.comp2601.message;

/**
 * Created by kyimoecho on 16-03-15.
 */
import java.io.Serializable;

public class Header implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -7729816603167728273L;
    public String sender;	// Identity of sender; e.g., Bob
    public String receiver;	// Identity of receiver; e.g., Lou
    public String command;	// Type of message e.g. login, data, ...

    public Header() {
        sender = Message.DEFAULT_SENDER;
        receiver = Message.DEFAULT_RECEIVER;
        command = Message.DEFAULT_TYPE;
    }


    public String toString(){
        return "\"header\": { \"sender\":\"" + sender + "\",\n\"receiver\":\"" + receiver + "\",\n\"command\": \"" + command + "\" }";
    }
}


