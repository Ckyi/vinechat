package com.zabu.kyimoecho.chatclient;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import com.comp2601.message.*;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;



import com.zabu.kyimoecho.chatclient.communication.*;

public class ChatActivity extends AppCompatActivity {

    private clientThreadWithReactor clientTWR;
    StartReactor myReactor;
    Handler handler;  //handler to allow updates to the UI
    ProgressDialog myProgressDialog;  //progress spinner
    ChatActivity chatActivityInstance;
    ArrayList<String> arraySpinner;
    Spinner spinner;
    EditText chatBox,msgEditText;
    Button sendButton,clearButton,leaveButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        State.instance = this;
        handler = new Handler();
        chatActivityInstance = this; //Initialize global variable with instance of this class
        chatBox = (EditText) findViewById(R.id.chatBoxEditText);
        msgEditText = (EditText)findViewById(R.id.userMessageeditText);

        //Initialize spinner control
        arraySpinner = new ArrayList<>();
        arraySpinner.add(State.EVERYONE);
        spinner = (Spinner) findViewById(R.id.activeUserSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, arraySpinner);
        spinner.setAdapter(adapter);

        //Initialize button click handlers
        sendButton = (Button) findViewById(R.id.sendButton);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendClicked(v);
            }

        });

        //Initialize button click handlers
        clearButton = (Button) findViewById(R.id.clearButton);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chatBox.setText("");
            }

        });

        //Initialize button click handlers
        leaveButton = (Button) findViewById(R.id.leaveButton);
        leaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leaveClicked(v);
            }

        });

        // Start reactor
        myReactor = new StartReactor();
        myReactor.execute("");

        //Connect to server
        Message msg = new Message();
        try {
            msg.header.sender = State.username;
            msg.header.receiver = State.EVERYONE;
            msg.header.command = State.CONNECT_REQUEST;
            msg.body.addField("msg","none");

            JSONObject jo = new JSONObject(msg.toString());

            new Write(jo).execute("");

            showDialog(0);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Send button click handler
    private void sendClicked(View v)
    {
        Message msgObj = new Message();
        try{
            String msgText = msgEditText.getText().toString().trim();

            msgObj.header.sender = State.username;

            String selection = spinner.getSelectedItem().toString();

            if(selection.equals(State.EVERYONE)) { //Check message is for private or for everyone
                msgObj.header.receiver = State.EVERYONE;
                msgObj.header.command = State.PUBLIC;
            }
            else{
                msgObj.header.receiver = selection;
                msgObj.header.command = State.PRIVATE;
            }
            msgObj.body.addField("msg",msgText);
            msgEditText.setText("");
            JSONObject jo = new JSONObject(msgObj.toString());
            new Write(jo).execute("");
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    // Click event handler for leave button
    private void leaveClicked(View v)
    {
        Message msgObj = new Message();
        try{

            msgObj.header.sender = State.username;
            msgObj.header.receiver = "ALL";
            msgObj.header.command = State.DISCONNECT_REQUEST;
            msgObj.body.addField("msg", " none");

            JSONObject jo = new JSONObject(msgObj.toString());
            new Write(jo).execute("");
            finish();
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    //setup for creating a Progress Dialog
    protected Dialog onCreateDialog(int id) {

        // Arg value corresponds to showDialog(0)
        if (id != 0)
            return null;
        myProgressDialog = new ProgressDialog(this);
        myProgressDialog.setMessage("Loading ...");
        myProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        myProgressDialog.setCancelable(false);
        return myProgressDialog ;
    }


    /*
     * This class actually creates the server with the reactor and sets up
     * reading etc.
     */
    public class StartReactor extends AsyncTask<String, String, String> {

        protected String doInBackground(String... params) {
            clientTWR = new clientThreadWithReactor();
            clientTWR.init();
            try {
                clientTWR.run();
            } catch (Exception e) {
                return "Failed to initialize server";
            }

            return "Initialized server";
        }

        protected void onPostExecute(String result) {
            System.out.println(result);
            super.onPostExecute(result);
        }
    }

    /*
     * Require this in order to do network I/O in background
     */
    public class Write extends AsyncTask<String, String, String> {
        JSONObject msg;

        public Write(JSONObject msg) {
            this.msg = msg;
        }

        protected String doInBackground(String... params) {
            try {
                clientTWR.myEventSource.write(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return msg.toString();
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    /*
     * Implementation of the reactor within an object class.
     */
    private class clientThreadWithReactor {
        public Reactor myReactor;
        public JSONEventSource myEventSource;

        //i
        public void init() {
            myReactor = new Reactor();
            myReactor.register(State.CONNECTED_RESPONSE, new ConnectedDisconnectedResponseHandler());
            myReactor.register(State.PUBLIC, new SendMessageResponseHandler());
            myReactor.register(State.PRIVATE, new SendMessageResponseHandler());
            myReactor.register(State.DISCONNECT_RESPONSE, new ConnectedDisconnectedResponseHandler());
        }

        void run() throws IOException, ClassNotFoundException, NoEventHandler,
                JSONException {
            Integer port = Integer.parseInt(State.selectedServer.getPort());
            myEventSource = new JSONEventSource(State.selectedServer.getHostIp(), port);
            ThreadWithReactor twr = new ThreadWithReactor(myEventSource, myReactor);
            twr.start();
        }

        /*
         * Event handler for the CONNECTED RESPONSE message
         */
        private class ConnectedDisconnectedResponseHandler implements EventHandler {
            public void handleEvent(Event event) {
                JSONEvent myJSONEvent = (JSONEvent)event;
                final Message msg = myJSONEvent.getMessage();

                handler.post(new Runnable() {
                    public void run() {
                        myProgressDialog.dismiss();
                        arraySpinner.clear();
                        try {
                            String bodyJsonStr = "{" + msg.body.toString() + "}";
                            JSONObject bodyWithBodyTag = new JSONObject(bodyJsonStr);
                            JSONObject body = new JSONObject(bodyWithBodyTag.getString("body"));

                            if(!body.getString("msg").equals("none")) {
                                String userList = body.getString("userlist");
                                String[] userListArray = userList.split("\\s+");

                                ArrayList<String> backingData = new ArrayList<>();
                                backingData.add(State.EVERYONE);
                                for (int i = 0; i < userListArray.length; i++) {
                                    if(!userListArray[i].equals(State.username))
                                        backingData.add(i, userListArray[i]);
                                }

                                ArrayAdapter<String> adapter = new ArrayAdapter<>(chatActivityInstance,
                                        R.layout.support_simple_spinner_dropdown_item, backingData);
                                spinner.setAdapter(adapter);
                                chatBox.append(body.getString("msg") + "\n");
                            }
                        }
                        catch(JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                });

            }
        }

        private class SendMessageResponseHandler implements EventHandler {
            public void handleEvent(Event event) {
                JSONEvent myJSONEvent = (JSONEvent)event;
                final Message msg = myJSONEvent.getMessage();

                handler.post(new Runnable() {
                    public void run() {
                        try {
                            String bodyJsonStr = "{" + msg.body.toString() + "}";
                            JSONObject bodyWithBodyTag = new JSONObject(bodyJsonStr);
                            JSONObject body = new JSONObject(bodyWithBodyTag.getString("body"));
                            if(msg.header.command.equals(State.PUBLIC))
                                chatBox.append(msg.header.sender + " : " + body.getString("msg") + "\n");
                            else
                                chatBox.append(msg.header.sender + "(Private) : " + body.getString("msg") + "\n");
                        }
                        catch(JSONException e)
                        {
                            e.printStackTrace();
                        }

                    }
                });

            }
        }



        }
}
