package com.zabu.kyimoecho.chatclient;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Button;
import android.widget.ArrayAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;


public class ConnectActivity extends AppCompatActivity {

    private ArrayAdapter adapter;
    private ListView listView;
    private Button addButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, State.listItems);
        listView = (ListView) findViewById(R.id.serverlist_listview);
        listView.setAdapter(adapter);

        //Handler initialization for list view
        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                itemClicked(parent, view, position, id);

            }
        });

        addButton = (Button) findViewById(R.id.addButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addClicked(v);
            }

        });

    }

    //add button click event handler
    private void addClicked(View v) {
        Intent serverAddIntent = new Intent(ConnectActivity.this, ServerAddActivity.class);
        startActivityForResult(serverAddIntent, State.NO_UPDATE_LIST);
    }

    //Item clicked even handler for connection list
    private void itemClicked(AdapterView<?> parent, View view, Integer position, Long id)
    {
        // ListView Clicked item value
        String serverName = (String) listView.getItemAtPosition(position);
        State.selectedServer = State.serverList.get(serverName);

        Intent serverAddIntent = new Intent(ConnectActivity.this, ChatActivity.class);
        startActivity(serverAddIntent);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == State.UPDATE_LIST)
        {
            adapter.notifyDataSetChanged();
        }
    }


}
