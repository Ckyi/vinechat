package com.zabu.kyimoecho.chatclient;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ServerAddActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serveradd);

        Button addButton = (Button) findViewById(R.id.addButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addClicked(v);
            }

        });
    }

    private void addClicked(View v)
    {
        EditText hostEditText = (EditText) findViewById(R.id.hostIPText);
        EditText portEditText = (EditText) findViewById(R.id.portNumberText);
        EditText serverNameEditText = (EditText) findViewById(R.id.serverNameText);

        String host = hostEditText.getText().toString().trim();
        String port = portEditText.getText().toString().trim();
        String serverName = serverNameEditText.getText().toString().trim();

        State.serverList.put(serverName,new ServerInfo(serverName,host,port));

        State.listItems.add(serverName);
        setResult(State.UPDATE_LIST);
        finish();
    }
}
