package com.zabu.kyimoecho.chatclient;

/**
 * Created by kyimoecho on 16-03-14.
 */

// Represents server connection
public class ServerInfo {
    private String hostIp;
    private String port;
    private String name;

    public ServerInfo(String name,String hostIp,String port)
    {
        this.hostIp = hostIp;
        this.port = port;
        this.name = name;
    }

    public String getHostIp() { return hostIp; }
    public String getName() { return name; }
    public String getPort() { return port; }
}
