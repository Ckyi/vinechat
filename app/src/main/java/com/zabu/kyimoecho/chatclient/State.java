package com.zabu.kyimoecho.chatclient;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by kyimoecho on 16-03-14.
 */

//This class contains constants and keep tracks of control current selections
public class State {
    public static HashMap<String,ServerInfo> serverList;
    public static ArrayList<String> listItems;
    public static String username;
    public static final Integer UPDATE_LIST = 1;
    public static final Integer NO_UPDATE_LIST = 0;
    public static ChatActivity instance;
    public static ServerInfo selectedServer;
    public static final String CONNECT_REQUEST = "CONNECT_REQUEST";
    public static final String DISCONNECT_REQUEST = "DISCONNECT_REQUEST";
    public static final String CONNECTED_RESPONSE = "CONNECTED_RESPONSE";
    public static final String DISCONNECT_RESPONSE = "DISCONNECT_RESPONSE";
    public static final String PUBLIC = "PUBLIC";
    public static final String PRIVATE = "PRIVATE";
    public static final String EVERYONE = "EVERYONE";


    // Initialization method
    public static void init()
    {
        serverList = new HashMap<>();
        listItems = new ArrayList<>();
    }
}
