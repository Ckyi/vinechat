package com.zabu.kyimoecho.chatclient.communication;

/**
 * Created by kyimoecho on 16-03-15.
 */
public class Event {
    public final String type;

    Event(String type) {
        this.type = type;
    }
}
