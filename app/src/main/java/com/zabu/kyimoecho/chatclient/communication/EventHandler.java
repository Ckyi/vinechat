package com.zabu.kyimoecho.chatclient.communication;

/**
 * Created by kyimoecho on 16-03-15.
 */
import java.io.IOException;

import org.json.JSONException;

public interface EventHandler {
    public void handleEvent(Event event) throws JSONException, IOException;
}

