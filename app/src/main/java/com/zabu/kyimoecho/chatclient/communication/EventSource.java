package com.zabu.kyimoecho.chatclient.communication;

/**
 * Created by kyimoecho on 16-03-15.
 */
import java.io.IOException;

import org.json.JSONException;

public interface EventSource {
    public Event getEvent() throws IOException, ClassNotFoundException, JSONException;
}
