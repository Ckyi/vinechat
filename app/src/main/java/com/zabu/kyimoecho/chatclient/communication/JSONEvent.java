package com.zabu.kyimoecho.chatclient.communication;

/**
 * Created by kyimoecho on 16-03-15.
 */
import org.json.JSONException;
import org.json.JSONObject;
import com.comp2601.message.*;

import org.json.JSONException;
import org.json.JSONObject;
import com.comp2601.message.*;

public class JSONEvent extends Event {
    private JSONEventSource es;
    //private JSONObject jo;
    private Message msg;

    public JSONEvent(Message msg, JSONEventSource es) throws JSONException {
        super(msg.header.command);
        this.es = es;
        //this.jo = jo;
        this.msg = msg;

		/*this.msg = new Message();

		this.msg.header.sender = getSender();
		this.msg.header.receiver = getReceiver();
		this.msg.header.command = getCommand();
		this.msg.body.addField("msg", getBody());*/
    }

    public Message getMessage() {return msg;}

    /*public String getCommand() throws JSONException{
        return jo.getString("command");
    }
    public String getBody() throws JSONException{
        return jo.getString("body");
    }
    public String getSender() throws JSONException{
        return jo.getString("sender");
    }
    public String getReceiver() throws JSONException{
        return jo.getString("receiver");
    }
    public String get(String key) {
        try {
            return jo.getString(key);
        } catch (JSONException e) {
            return null;
        }
    }*/
    public JSONEventSource getES() {
        return es;
    }
}


