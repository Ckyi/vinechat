package com.zabu.kyimoecho.chatclient.communication;

/**
 * Created by kyimoecho on 16-03-15.
 */


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import org.json.JSONException;
import org.json.JSONObject;
import com.comp2601.message.*;

public class JSONEventSource implements EventSource {
    BufferedWriter writer;
    BufferedReader reader;
    Socket s;
    int no;

    public JSONEventSource(String host, int port) throws UnknownHostException,
            IOException {
        OutputStreamWriter osw;
        InputStreamReader isw;

        s = new Socket(host, port);
        osw = new OutputStreamWriter(s.getOutputStream());

        writer = new BufferedWriter(osw);
        isw = new InputStreamReader(s.getInputStream());
        reader = new BufferedReader(isw);
        no = 0;

    }

    public JSONEventSource(Socket s) throws UnknownHostException, IOException {
        reader = new BufferedReader(new InputStreamReader(s.getInputStream()));
        writer = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
        no = 0;
    }

    public Event getEvent() throws IOException, ClassNotFoundException,
            JSONException {
        StringBuffer buf = new StringBuffer();
        String line;
        boolean done = false;
        while (!done) {
            line = reader.readLine();
            if (line == null || line.isEmpty())
                done = true;
            else {
                no++;
                System.out.println("["+no+"]"+line);
                buf.append(line);
            }
        }
        if (buf.length()==0)
            return null;
        JSONObject jo = new JSONObject(buf.toString());

        Message msg = jsonToMessage(jo);
        return new JSONEvent(msg, this);
    }

    private Message jsonToMessage(JSONObject jsonObj) throws JSONException
    {
        JSONObject header = jsonObj.getJSONObject("header");
        JSONObject body = jsonObj.getJSONObject("body");

        Message msg = new Message();
        msg.header.sender = header.getString("sender");
        msg.header.receiver = header.getString("receiver");
        msg.header.command = header.getString("command");
        msg.body.addField("msg", body.getString("msg"));

        if(body.has("userlist"))
            msg.body.addField("userlist", body.getString("userlist"));

        return msg;
    }

    public void write(JSONObject msg) throws IOException {
        writer.write(msg.toString()+"\n");
        writer.write("\n");
        writer.flush();
    }
}
