package com.zabu.kyimoecho.chatclient.communication;

/**
 * Created by kyimoecho on 16-03-15.
 */
public class NoEventHandler extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 6708823595103392249L;

    public NoEventHandler(String msg) {
        super(msg);
    }

}

