package com.zabu.kyimoecho.chatclient.message;

/**
 * Created by kyimoecho on 16-03-15.
 */


public class Body {
    /**
     *
     */
    private String data;

    Body(String data) {
        this.data = data;
    }

    public  String toString(){
        String s = "BODY:\n";
        s += this.data;
        return s;
    }
}



