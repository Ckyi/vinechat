package com.zabu.kyimoecho.chatclient.message;

/**
 * Created by kyimoecho on 16-03-15.
 */


public class Header{
    /**
     *
     */
    private static final long serialVersionUID = -7729816603167728273L;
    public String sender;    // Identity of sender; e.g., Bob
    public String receiver;    // Identity of receiver; e.g., Lou
    public String type;    // Type of message e.g. login, data, ...

    public Header(String sender, String receiver, String type) {
        this.sender = sender;
        this.receiver = receiver;
        this.type = type;
    }


    public String getSender() {
        return this.sender;
    }

    public String getReceiver() {
        return this.receiver;
    }

    public String getType() {
        return this.type;
    }

    public String toString() {
        return "HEADER: sender:" + sender + " receiver:" + receiver + " type: " + type;
    }
}
