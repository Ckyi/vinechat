package com.zabu.kyimoecho.chatclient.message;

/**
 * Created by kyimoecho on 16-03-15.
 */
import java.io.Serializable;

public class Message implements Serializable {
    /**
     *
     */
    //message types
    public static final String DATA = "data";
    public static final String LOGIN = "login";
    public static final String LOGOUT = "logout";
    public static final String DISCONNECT = "disconnect";

    //default header fields
    /*public static final String DEFAULT_SENDER = "unknown";
    public static final String DEFAULT_RECEIVER = "unknown";
    public static final String DEFAULT_TYPE = "unknown";
*/

    public Header header;
    public Body body;
    public String sender;
    public String receiver;

    public Message(String sender, String receiver) {
       // header = new Header(sender,receiver);
        //body = new Body();
        this.sender = sender;
        this.receiver = receiver;
    }

    public String toString(){
        return header.toString() + "\n" + body.toString();
    }
}

